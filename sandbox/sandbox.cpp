/* 
	Pour tester le programme : 
	-hash monHash -algo sha1 -alphabet monAlphabet -chunksize 12 
*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <thread>

// Librairies des divers Hash
#include <CHashMd5.h>
#include <CHashCrc32.h>
#include <CHashNone.h>
#include <CHashSha1.h>
#include <CHashSha224.h>
#include <CHashSha256.h>

//Gestion touche ESC
#include <CUtil.h>

// Lire / Ecrire fichier
#include <fstream>

#include <CDateTime.h>

// Pour le cast
#include <sstream>

using namespace std;

void sandbox() {

	/*
	Code exemple � adapter dans la boucle de brute force
	Permet de terminer le traitement si touche ESC
	Enregistre la derni�re valeur dans le fichier status.txt
	Red�marre la boucle depuis la derni�re valeur
	*/

	int i = 0, dernierMdp;
	string vFichier;

	// Lecture du fichier
	ifstream fichier("status.txt", ios::in);

	if (!fichier.fail()) {
		getline(fichier,vFichier); // Recup�re le contenu
	}

	// Cast string en int
	/* Ne pas appliquer dans programme final puisque le pass est un string*/
	istringstream(vFichier) >> dernierMdp;

	for (i = dernierMdp; i < i + 1; i++) {
		cout << i << endl;
		if (CUtil::IsEscKeyPressed()) {
			// �criture dans le fichier avec effacement de la valeur pr�c�dente 
			ofstream fichier("status.txt", ios::out | ios::trunc);
			fichier << i;
			fichier.close();
			break;
		}
	}
	return;
}


int main(int argc, char *argv[])
{
	string hash, algo, alphabet, chunksize;

	std::string arguments(argv[1]);


	hash = argv[2];

	algo = argv[4];

	if (algo == "md5") {
		CHashMd5::CHashMd5();
	}
	if (algo == "crc32") {
		CHashCrc32::CHashCrc32();
	}
	if (algo == "none") {
		CHashNone::CHashNone();
	}
	if (algo == "sha1") {
		CHashSha1::CHashSha1();
	}
	if (algo == "sha224") {
		CHashSha224::CHashSha224();
	}
	if (algo == "sha256") {
		CHashSha256::CHashSha256();
	}

	alphabet = argv[6];
	chunksize = argv[8];

	// Appel de la fonction dans un thread
	std::thread mainThread(sandbox);
	mainThread.join();

	// Informations pour log
	std::thread::id idThread = std::this_thread::get_id();

	ofstream logs("dhc.log", ios::out | ios::trunc);
	logs << "ID thread : " << idThread << "\n";
	//logs << "DateTime : " << *ptr << "\n";
	logs.close();

	return 0;
}
